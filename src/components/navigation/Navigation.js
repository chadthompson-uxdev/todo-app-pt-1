import React from 'react'
import  {NavLink, Link}from 'react-router-dom'
function Navigation() {
    return (
        <ul>
        <li>
            <Link to="/">All</Link>
        </li>
        <li>
            <NavLink to="/active" activeStyle={{
background:'white',
color:'red'
}}>Active</NavLink>
        </li>
        <li>
            <NavLink to="/completed" activeStyle={{
background:'white',
color:'red'
}}>Completed</NavLink>
        </li>
        </ul>
    );
}
export default Navigation;