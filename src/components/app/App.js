import React, { Component } from "react";
import todosList from "../../todos.json";
import TodoList from "../todoList/TodoList"
import Navigation from "../navigation/Navigation"
import {
  BrowserRouter as Router,
  NavLink,
  Route,
  Link
} from "react-router-dom";
import TodoItem from "../todoItem/TodoItem";


class App extends Component {
  state = {
    todos: todosList
  };

handleAddTodo = event=>{
  if (event.key==="Enter"){
    console.log("fish")
    const addData = {
      userId: 1,
      id: Math.floor(Math.random()*1000),
      title: event.target.value,
      completed: false
    }
    const enterTodos =this.state.todos.slice()
    enterTodos.push(addData)
    this.setState({todos:enterTodos})
    event.target.value= ""
  }
}

handleDelete = (event, id) => {
  const newTodo = this.state.todos.filter(
    todo => todo.id !== id
  )
  this.setState({ todos: newTodo })
}

handleClearCompleted = (event) =>{
  const cleartodo =this.state.todos.filter(
    todo=> !todo.completed
  )
  this.setState({todos:cleartodo})
}

handleToggle = (event, id)=>{
  console.log('event')
  const makeCompleted = this.state.todos.map( 
  todo => {if(todo.id === id){
  return{
...todo,
    completed : !todo.completed 
  }
}
return (todo)
})  
  this.setState({todos:makeCompleted})
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" onKeyDown ={this.handleAddTodo} placeholder="What needs to be done?" autoFocus />
        </header>

<Route exact path="/" 
render={()=>(
   <TodoList
todos={this.state.todos}
handleDelete = {this.handleDelete}
handleClearCompleted = {this.handleClearCompleted}
handleToggle={this.handleToggle}
/>
)
}
/>
<Route path="/active"
render={()=>(
<TodoList 
todos={this.state.todos.filter((todo)=>todo.completed===false)}
handleDelete = {this.handleDelete}
handleClearCompleted = {this.handleClearCompleted}
handleToggle={this.handleToggle}
/>
)
}/>
<Route path="/completed"
render={()=>(
<TodoList
todos={this.state.todos.filter((todo)=>todo.completed===true)}
handleDelete = {this.handleDelete}
handleClearCompleted = {this.handleClearCompleted}
handleToggle={this.handleToggle}
/>
)
}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>{
              this.state.todos.filter((todo)=>todo.completed !== true).length}
              </strong> item(s) left
          </span>
          <ul className="filters">
<Navigation/>
</ul>
          <button onClick={this.handleClearCompleted} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
   };
 }
    
export default App