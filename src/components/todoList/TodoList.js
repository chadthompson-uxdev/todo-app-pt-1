import React, {Component} from 'react'
import TodoItem from "../todoItem/TodoItem"

class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map(todo => (
              <TodoItem key={todo.userId} title={todo.title} completed={todo.completed} id={todo.id} handleDelete={this.props.handleDelete} handleToggle={this.props.handleToggle} />
            ))}
          </ul>
        </section>
      );
    }
  }
  
  export default TodoList