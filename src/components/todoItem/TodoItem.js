import React, {Component} from 'react'

class TodoItem extends Component {
    render() {
      return (
        <li className={this.props.completed ? "completed" : ""}>
          <div className="view">
            <input className="toggle" type="checkbox"  onClick={event =>this.props.handleToggle(event, this.props.id)} />
            <label>{this.props.title}</label>
            <button onClick={event =>this.props.handleDelete(event, this.props.id)} className="destroy"/>
          </div>
        </li>
      );
    }
  }

  export default TodoItem